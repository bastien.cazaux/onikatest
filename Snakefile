import os

path_base = "."
path_envs = "Envs/"
path_data = "Datas/"
path_ref = "Refs/"
path_query = "Queries/"
path_fof = "Fofs/"
path_distance = "Dist/"
path_plot = "Plot/"

path_querySim = "python Codes/part.py"
path_pythonPlot = "python Codes/plot.py"
path_pythonPlotDiff = "python Codes/plot_diff.py"
path_pythonPlotOnika = "python Codes/plot_onika.py"
path_pythonPlotOnikaByError = "python Codes/plot_onika_by_error.py"
path_pythonGetOnikaDiff = "python Codes/get_onika_diff.py"
path_pythonPlotDiffAll = "python Codes/plot_diff_all.py"
path_queryfofSimka = "python Codes/fofsimka.py"

# path_refSimulator = "/home/malfoy/devel/BRAW/refSimulator"
# path_onika = "/home/malfoy/devel/Onika/src/onika"
path_refSimulator = "/home/cazaux/Git/GitAux/BRAW/refSimulator"
path_onika = "/home/cazaux/Git/ONIKA/Appli/bin/onika"
path_niqki = "/home/cazaux/Git/NIQKI/Appli/bin/niqki"

# simka -in ecolic_10_simka.fof -out distance_matrix_simka -out-tmp distance_matrix_simka_aux -simple-dist -complex-dist
# dashing dist -Odistance_matrix_dashing.txt -osize_estimates.txt -Fecolic_10.fof




rule all:
    input:
        expand(os.path.join(path_plot,"{plot}_{genome}_{key}_{max_error_refs}_{max_percent_queries}.png"),plot=["plot","plot-diff","plot-onika","plot-onika-by-error"],genome=["ecoli"],max_error_refs=0.05,max_percent_queries=0.01,key=list(range(1))),
        expand(os.path.join(path_plot,"plot-all-diff_{genome}_{nb}_{max_error_refs}_{max_percent_queries}.png"), genome=["ecoli"], max_error_refs=0.05,max_percent_queries=0.01,nb=10),

rule refSim:
    input:
        os.path.join(path_data,"{genome}.fa.gz")
    output:
        os.path.join(path_ref,"{genome}_{key}_{rate}.fa.gz")
    shell:
        path_refSimulator + " {input} {wildcards.rate} {output}"


rule querySim:
    input:
        os.path.join(path_data,"{genome}.fa.gz")
    output:
        os.path.join(path_query,"{genome}_{key}_{rate}.fa.gz")
    shell:
        path_querySim + " {input} {wildcards.rate} {output}"

def all_liste_ref(wildcards):
    nb_refs = 10
    nb_queries = 30
    return expand(os.path.join(path_ref,"{genome}_{key}_{rate}.fa.gz"),genome=wildcards.genome,key=wildcards.key,rate=(f'{ x*(float(wildcards.max_error_refs))/float(nb_refs-1):.4f}' for x in range(nb_refs))) + expand(os.path.join(path_query,"{genome}_{key}_{rate}.fa.gz"),genome=wildcards.genome,key=wildcards.key,rate=(f'{1 + x*(float(wildcards.max_percent_queries)-1)/float(nb_queries-1):.4f}' for x in range(nb_queries)))


rule fileRefSim:
    input:
        all_liste_ref
    output:
        os.path.join(path_fof,"{genome}_{key}_{max_error_refs}_{max_percent_queries}.fof")
    shell:
        "realpath --relative-to " + path_fof + " {input} > {output}"

rule fileRefSimSimka:
    input:
        os.path.join(path_fof,"{genome}_{key}_{max_error_refs}_{max_percent_queries}.fof")
    output:
        os.path.join(path_fof,"{genome}_{key}_{max_error_refs}_{max_percent_queries}_simka.fof")
    conda:
        os.path.join(path_envs,"biopython.yaml")
    shell:
        path_queryfofSimka + " {input} {output}"

rule dashing:
    input:
        os.path.join(path_fof,"{genome}_{key}_{max_error_refs}_{max_percent_queries}.fof")
    params:
        input="{genome}_{key}_{max_error_refs}_{max_percent_queries}.fof",
        output=os.path.join("..",path_distance,"dist_{genome}_{key}_{max_error_refs}_{max_percent_queries}.dashing")
    output:
        os.path.join(path_distance,"dist_{genome}_{key}_{max_error_refs}_{max_percent_queries}.dashing")
    conda:
        os.path.join(path_envs,"dashing.yaml")
    shell:
        "cd " + path_fof + ";" + "dashing dist -S 15 -O {params.output} -F {params.input};" + "cd -;"

rule onika:
    input:
        os.path.join(path_fof,"{genome}_{key}_{max_error_refs}_{max_percent_queries}.fof")
    output:
        os.path.join(path_distance,"dist_{genome}_{key}_{max_error_refs}_{max_percent_queries}.onika")
    params:
        input="{genome}_{key}_{max_error_refs}_{max_percent_queries}.fof",
        outputgz=os.path.join("..",path_distance,"dist_{genome}_{key}_{max_error_refs}_{max_percent_queries}.onika.gz")
    shell:
        "cd " + path_fof + ";" + path_onika +" -S 15 -W 8 -I {params.input} -Q  {params.input} -O {params.outputgz};" + "gzip -d {params.outputgz};" + " cd -;"

rule niqki:
    input:
        os.path.join(path_fof,"{genome}_{key}_{max_error_refs}_{max_percent_queries}.fof")
    output:
        os.path.join(path_distance,"dist_{genome}_{key}_{max_error_refs}_{max_percent_queries}.niqki")
    params:
        input="{genome}_{key}_{max_error_refs}_{max_percent_queries}.fof",
        outputgz=os.path.join("..",path_distance,"dist_{genome}_{key}_{max_error_refs}_{max_percent_queries}.niqki.gz")
    shell:
        "cd " + path_fof + ";" + path_niqki +" -S 15 -W 8 -I {params.input} -Q  {params.input} -O {params.outputgz};" + "gzip -d {params.outputgz};" + " cd -;"

rule simka:
    input:
        os.path.join(path_fof,"{genome}_{key}_{max_error_refs}_{max_percent_queries}_simka.fof")
    output:
        directory(os.path.join(path_distance,"{genome}_{key}_{max_error_refs}_{max_percent_queries}_simka"))
    conda:
        os.path.join(path_envs,"simka.yaml")
    shell:
        "simka -kmer-size 31 -abundance-min 1 -in {input} -out {output} -out-tmp {output}_out -simple-dist -complex-dist; rm -r {output}_out; for x in {output}/*; do gzip -d $x; done"

rule simka_ab_jaccard:
    input:
        directory(os.path.join(path_distance,"{genome}_{key}_{max_error_refs}_{max_percent_queries}_simka"))
    params:
        csv=os.path.join(path_distance,"{genome}_{key}_{max_error_refs}_{max_percent_queries}_simka","mat_presenceAbsence_jaccard.csv"),
    output:
        os.path.join(path_distance,"dist_{genome}_{key}_{max_error_refs}_{max_percent_queries}.simkaPAJaccard")
    shell:
        "cp {params.csv} {output};"

rule plot:
    input:
        expand(os.path.join(path_distance,"dist_{{genome}}_{{key}}_{{max_error_refs}}_{{max_percent_queries}}.{software}"),software=["dashing","onika","simkaPAJaccard","niqki"])
    output:
        os.path.join(path_plot,"plot_{genome}_{key}_{max_error_refs}_{max_percent_queries}.png")
    conda:
        os.path.join(path_envs,"matplotlib.yaml")
    shell:
        path_pythonPlot + " {output} {input}"

rule plot_diff:
    input:
        expand(os.path.join(path_distance,"dist_{{genome}}_{{key}}_{{max_error_refs}}_{{max_percent_queries}}.{software}"),software=["dashing","onika","simkaPAJaccard","niqki"])
    output:
        os.path.join(path_plot,"plot-diff_{genome}_{key}_{max_error_refs}_{max_percent_queries}.png")
    conda:
        os.path.join(path_envs,"matplotlib.yaml")
    shell:
        path_pythonPlotDiff + " {output} {input}"

rule plot_onika:
    input:
        expand(os.path.join(path_distance,"dist_{{genome}}_{{key}}_{{max_error_refs}}_{{max_percent_queries}}.{software}"),software=["dashing","onika","simkaPAJaccard","niqki"])
    output:
        os.path.join(path_plot,"plot-onika_{genome}_{key}_{max_error_refs}_{max_percent_queries}.png")
    conda:
        os.path.join(path_envs,"matplotlib.yaml")
    shell:
        path_pythonPlotOnika + " {output} {input}"

rule plot_onika_by_error:
    input:
        expand(os.path.join(path_distance,"dist_{{genome}}_{{key}}_{{max_error_refs}}_{{max_percent_queries}}.{software}"),software=["dashing","onika","simkaPAJaccard","niqki"])
    output:
        os.path.join(path_plot,"plot-onika-by-error_{genome}_{key}_{max_error_refs}_{max_percent_queries}.png")
    conda:
        os.path.join(path_envs,"matplotlib.yaml")
    shell:
        path_pythonPlotOnikaByError + " {output} {input}"

rule get_onika_diff:
    input:
        expand(os.path.join(path_distance,"dist_{{genome}}_{{key}}_{{max_error_refs}}_{{max_percent_queries}}.{software}"),software=["dashing","onika","simkaPAJaccard","niqki"])
    output:
        os.path.join(path_plot,"get-onika-diff_{genome}_{key}_{max_error_refs}_{max_percent_queries}.csv")
    conda:
        os.path.join(path_envs,"matplotlib.yaml")
    shell:
        path_pythonGetOnikaDiff + " {output} {input}"

def f_diff_all(wildcards):
    return expand(os.path.join(path_plot, "get-onika-diff_{{genome}}_{key}_{{max_error_refs}}_{{max_percent_queries}}.csv"), key=list(range(int(wildcards.nb))))

rule plot_diff_all:
    input:
        f_diff_all
    output:
        os.path.join(path_plot,"plot-all-diff_{genome}_{nb}_{max_error_refs}_{max_percent_queries}.png")
    conda:
        os.path.join(path_envs,"matplotlib.yaml")
    shell:
        path_pythonPlotDiffAll + " {output} {input}"

rule clean:
    shell:
        "rm " + os.path.join(path_plot,"*") + ";" + "rm " + os.path.join(path_fof,"*") + ";" + "rm -r " + os.path.join(path_distance,"*") + ";" + "rm " + os.path.join(path_query,"*") + ";"  + "rm " + os.path.join(path_ref,"*") + ";"

#
