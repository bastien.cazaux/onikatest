import sys

filename_input = sys.argv[1]
filename_ouput = sys.argv[2]



from Bio import SeqIO
import gzip

path_in = filename_input
path_out = filename_ouput
handle_in = gzip.open(path_in, "rt")
handle_out = gzip.open(path_out, "wt")

fq = SeqIO.parse(handle_in, "fasta")
for read in fq:
    title = read.format("fasta").split("\n")[0]+"\n"
    autre = ''.join(read.format("fasta").split("\n")[1:])+"\n"
    handle_out.write(title+autre)

handle_in.close()
handle_out.close()
