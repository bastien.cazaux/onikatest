import csv
import os
import sys
import matplotlib.pyplot as plt

def getGoodName(name):
    debut = os.path.split(os.path.split(name)[0])[1]
    fin = os.path.splitext(os.path.splitext(os.path.split(name)[1])[0])[0]
    return debut + "_" + fin

def getDashing(filename):
    d = {}
    with open(filename, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter='\t')
        li = []
        for (i,row) in enumerate(spamreader):
            if i == 0:
                li = [getGoodName(x) for x in row]
            else:
                d[getGoodName(row[0])] = {}
                for j in range(1,len(li)):
                    if row[j] == "-":
                        if getGoodName(row[0]) == li[j]:
                            d[getGoodName(row[0])][li[j]] = 1
                        else:
                            d[getGoodName(row[0])][li[j]] = d[li[j]][getGoodName(row[0])]
                    else:
                        d[getGoodName(row[0])][li[j]] = 1 - float(row[j])
    return d

def getOnika(filename):
    d = {}
    with open(filename, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter='\t')
        li = []
        for (i,row) in enumerate(spamreader):
            # print(row)
            if i == 0:
                li = [row[0].split(' ')[0]]+[getGoodName(x) for x in row if x != '']
            else:
                nom = getGoodName(row[0])
                d[nom] = {}
                for j in range(1,len(li)):
                    if row[j] == "-":
                        d[nom][li[j]] = 0
                    else:
                        d[nom][li[j]] = float(row[j])

    # print(filename,d)
    # for x in d.keys():
    #     print("\t" + x)
    #     for y in d[x].keys():
    #         print("\t\t" + y + " " + str(d[x][y]))
    # exit(0)
    return d

def getSimka(filename):
    d = {}
    with open(filename, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';')
        li = []
        for (i,row) in enumerate(spamreader):
            if i == 0:
                li = row
            else:
                d[row[0]] = {}
                for j in range(1,len(li)):
                    d[row[0]][li[j]] = float(row[j])
    return d

def getNiqki(filename):
    d = {}
    with open(filename, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=' ')
        li = []
        for (i,row) in enumerate(spamreader):
            d[getGoodName(row[0])] = {}
            for m in row[1:]:
                li = m.split(':')
                if len(li) == 2:
                    query,score = li
                    d[getGoodName(row[0])][getGoodName(query)] = 1 - float(score)
    return d



di = {}
file_dashing = ""
file_onika = ""
file_simka = ""
file_niqki = ""

for filename in sys.argv[2:]:
    d = {}
    ext = os.path.splitext(filename)[1]
    nom = os.path.split(filename)[1]
    if ext == ".dashing":
        d = getDashing(filename)
        file_dashing = nom


    if ext == ".onika":
        d = getOnika(filename)
        file_onika = nom

    if ext == ".simkaPAJaccard":
        d = getSimka(filename)
        file_simka = nom


    if ext == ".niqki":
        d = getNiqki(filename)
        file_niqki = nom



    di[nom] = d

# Aggregation
li_filename = list(di.keys())

da = {}
for filename in di.keys():
    for x in di[filename]:
        for y in di[filename][x]:
            if x != "_" and y != "_" and not "Ref" in x and not "Que" in y:
                if not x in da.keys():
                    da[x] = {}
                if not y in da[x].keys():
                    da[x][y] = {}
                da[x][y][filename] = di[filename][x][y]
            else:
                # print(x,y)
                pass

li_ref = [y for y in da[list(da.keys())[0]].keys()]

li_ref.sort()
y = li_ref[0]


fig, ax1 = plt.subplots()
diff_onika = [abs(da[x][y][file_onika] - da[x][y][file_simka])/(1-da[x][y][file_simka]) for x in da.keys()]
diff_dashing = [abs(da[x][y][file_dashing] - da[x][y][file_simka])/(1-da[x][y][file_simka]) for x in da.keys()]
diff_niqki = [abs(da[x][y][file_niqki] - da[x][y][file_simka])/(1-da[x][y][file_simka]) for x in da.keys()]
xx = [float(x.split('_')[-1]) for x in da.keys()]
ax1.plot(xx,diff_onika,label="Onika")
ax1.plot(xx,diff_dashing,label="Dashing")
ax1.plot(xx,diff_niqki,label="Niqki")
ax1.legend(loc='center left', bbox_to_anchor=(1, 0.5))
ax1.set_ylabel(y.split("_")[-1])
ax1.yaxis.set_tick_params(labelsize=8)
ax1.xaxis.set_tick_params(labelsize=8)
# exit(0)
#
# fig, ax = plt.subplots(len(li_ref))
#
# for (line,y) in enumerate(li_ref):
#     diff_onika = [abs(da[x][y][file_onika] - da[x][y][file_simka])/(1-da[x][y][file_simka]) for x in da.keys()]
#     diff_dashing = [abs(da[x][y][file_dashing] - da[x][y][file_simka])/(1-da[x][y][file_simka]) for x in da.keys()]
#     xx = [float(x.split('_')[-1]) for x in da.keys()]
#     ax[line].plot(xx,diff_onika,label="Onika")
#     ax[line].plot(xx,diff_dashing,label="Dashing")
#     ax[line].legend(loc='center left', bbox_to_anchor=(1, 0.5))
#     ax[line].set_ylabel(y.split("_")[-1])
#     ax[line].yaxis.set_tick_params(labelsize=8)
#     ax[line].xaxis.set_tick_params(labelsize=8)

# exit(0)
#
#
#
# for (line,x) in enumerate(da.keys()):
#     # # print(da[x])
#     # for y in da[x].keys():
#     #     print("\t" + y)
#     #     for f in da[x][y].keys():
#     #         print("\t\t" + f + str(da[x][y][f]))
#
#     dx = 10
#     width = 0.1*dx  # the width of the bars
#     multiplier = 0
#     for f in li_filename:
#         ll = [da[x][y][f] for y in da[x].keys()]
#         ly = [y for y in da[x].keys()]
#         offset = width * multiplier
#         rects = ax[line].bar([j*dx + offset for j in range(len(ll))], ll, width, label=f)
#         # ax[line].bar_label(rects, padding=3)
#         multiplier += 1
#
#         # print(f,ll)
#     ax[line].set_ylabel(x.split("_")[-1])
#     ax[line].yaxis.label.set(rotation='horizontal', ha='right');
#     # ax[line].set_title('')
#     # print([j + width for j in range(len(ll))])
#     ax[line].set_xticks([j*dx + width for j in range(len(ll))])
#     ax[line].set_xticklabels([y.split("_")[-1] for y in ly])
#     if line == 0:
#         ax[line].legend(loc='center left', bbox_to_anchor=(1, 0.5))
#     ax[line].set_ylim(0, 1)
#
#
fig.suptitle('Evolution of the difference with Simka by % of ref (no error)', fontsize=16)


filename_out = sys.argv[1]


plt.savefig(filename_out, dpi=500,bbox_inches='tight')
# exit(0)

# plt.show()



#
