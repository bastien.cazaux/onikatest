import csv
import os
import sys
import matplotlib.pyplot as plt
import pprint
from statistics import mean, stdev, quantiles

d = {}
for filename in sys.argv[2:]:
    f = open(filename)
    l = [[float(y) for y in x.strip().split("\t")] for x in f.readlines()]
    d[filename] = {}
    d[filename]["x"] = l[0]
    d[filename]["Onika"] = l[1]
    d[filename]["Dashing"] = l[2]
    d[filename]["Niqki"] = l[3]

li_x = list(d.values())[0]["x"]

da = {}
for (i,x) in enumerate(li_x):
    da[x] = {}
    da[x]["Onika"] = []
    da[x]["Dashing"] = []
    da[x]["Niqki"] = []
    for filename in d:
        da[x]["Onika"].append(d[filename]["Onika"][i])
        da[x]["Dashing"].append(d[filename]["Dashing"][i])
        da[x]["Niqki"].append(d[filename]["Niqki"][i])

l_moyen_onika = [mean(da[x]["Onika"]) for x in da.keys()]
# err_onika = [stdev(da[x]["Onika"]) for x in da.keys()]
l_moyen_dashing = [mean(da[x]["Dashing"]) for x in da.keys()]
l_moyen_niqki = [mean(da[x]["Niqki"]) for x in da.keys()]
# err_dashing = [stdev(da[x]["Dashing"]) for x in da.keys()]
err_onika = [[min(da[x]["Onika"]) for x in da.keys()],[max(da[x]["Onika"]) for x in da.keys()]]
err_dashing = [[min(da[x]["Dashing"]) for x in da.keys()],[max(da[x]["Dashing"]) for x in da.keys()]]
err_niqki = [[min(da[x]["Niqki"]) for x in da.keys()],[max(da[x]["Niqki"]) for x in da.keys()]]
# err_dashing = [stdev(da[x]["Dashing"]) for x in da.keys()]

diff = float(li_x[1]-li_x[0])/6
fig, ax1 = plt.subplots()

ax1.errorbar(li_x, l_moyen_onika, yerr=err_onika, label="Onika")
ax1.errorbar([x+diff for x in li_x], l_moyen_dashing, yerr=err_dashing, label="Dashing")
ax1.errorbar([x+diff*2 for x in li_x], l_moyen_niqki, yerr=err_niqki, label="Niqki")
ax1.legend()


filename_out = sys.argv[1]

plt.savefig(filename_out, dpi=500,bbox_inches='tight')
# plt.show()
