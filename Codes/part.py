import sys
import random


filename_input = sys.argv[1]
rate = float(sys.argv[2])
filename_ouput = sys.argv[3]



from Bio import SeqIO
import gzip

path_in = filename_input
path_out = filename_ouput
handle_in = gzip.open(path_in, "rt")
handle_out = gzip.open(path_out, "wt")

fq = SeqIO.parse(handle_in, "fasta")
for read in fq:
    title = read.format("fasta").split("\n")[0]+"\n"
    seq = ''.join(read.format("fasta").split("\n")[1:])
    taille = len(seq)
    new_taille = int(len(seq)*rate)
    position = random.randint(0, taille-new_taille)
    new_seq = seq[position:position+new_taille]
    handle_out.write(title+new_seq+"\n")

handle_in.close()
handle_out.close()
