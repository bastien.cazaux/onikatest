import sys
import os

filename = sys.argv[1]
filename_output = sys.argv[2]

f = open(filename)
f_output = open(filename_output, "w")

for line in f.readlines():
    line=line.strip()
    filefagz = os.path.split(line)[1]
    path = os.path.split(line)[0]
    dir = os.path.split(path)[1]
    filefa = os.path.splitext(filefagz)[0]
    file = os.path.splitext(filefa)[0]
    f_output.write(dir + "_" + file + " : " + line + "\n")

f.close()
f_output.close()
